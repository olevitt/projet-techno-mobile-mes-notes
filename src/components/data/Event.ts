export interface Event {
  title: string,
  start: string,
  end: string,
  allDay?: boolean,
  resource?: any
}
import React, { useState } from 'react';
import {Data} from './data/Data';
import { IonFab, IonFabButton, IonIcon, IonAlert, IonPage, IonHeader, IonToolbar, IonTitle, IonContent, IonItem, IonTextarea, IonInput, IonButton, IonText } from '@ionic/react';
import {  addOutline, arrowBackOutline } from 'ionicons/icons';
import ListeSimple from './ListeSimple';

interface Props {
  list: Data[];
}


const NewElement = ({list} : Props) => {

  const [createElement, setCreateElement] = useState(false);
  const [titre, setTitre] = useState<string>();
  const [description, setDescription] = useState<string>();
/*
  return (
  <>
    <IonFab vertical="bottom" horizontal="end" slot="fixed">
      <IonFabButton color="danger" onClick={() => {
          setCreateElement(true)
      }}>
        <IonIcon icon={addOutline} />
      </IonFabButton>
    </IonFab>

    <IonAlert
      isOpen={createElement}
        onDidDismiss={() => setCreateElement(false)}
      header={'Nouvelle note'}
        inputs={[
          {
            name: 'titre',
            type: 'text',
            placeholder: "Entrez le titre ici"
          },
          {
            name: 'description',
            type: 'text',
            placeholder: "Entrez votre texte ici"
          }
        ]}
      buttons={[
        {
          text: 'Annuler',
          role: 'cancel',
          cssClass: 'secondary',
          handler: () => {
            console.log('Confirm Cancel');
          }
        },
        {
          text: 'Enregistrer',
          handler: (data) => {
            console.log('Confirm OK ' + data.titre);
            list.push({
              titre: data.titre,
              description: data.description,
              date: "12-05-20",
              isRappelActivated: false,
              dateRappel: "",
              cheminImage: ""});
          }
        }
      ]}
    />
  </>
  )
  */
 return (
    <>
      <IonItem color="light">Nouvelle note</IonItem>

      <IonItem>
        <IonInput 
          value={titre} 
          type="text"
          placeholder="Entrez le titre ici..." 
          onIonChange={e => setTitre(e.detail.value!)}
          ></IonInput>
      </IonItem>

      <IonItem>
        <IonTextarea 
          value={description} 
          onIonChange={e => setDescription(e.detail.value!)}
          placeholder="Entrez votre texte ici..." 
          id="description"
          ></IonTextarea>
      </IonItem>
      
      <IonButton color="light" onClick={() => {
        list.push({
          titre: titre,
          description: description,
          date: "12-05-20",
          isRappelActivated: false,
          dateRappel: "",
          cheminImage: ""
        })
        }}>
         Ajouter
      </IonButton>

    </>
 )
};
  
  export default NewElement;

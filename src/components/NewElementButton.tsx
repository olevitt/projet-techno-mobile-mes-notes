import React, { useState } from "react";
import { Data } from "./data/Data";
import {
  IonFab,
  IonFabButton,
  IonIcon,
  IonAlert,
  IonPage,
  IonHeader,
  IonToolbar,
  IonTitle,
  IonContent,
  IonItem,
  IonTextarea,
  IonInput,
  IonButton,
  IonText,
  IonRouterOutlet,
} from "@ionic/react";
import { addOutline, arrowBackOutline } from "ionicons/icons";
import Tab1 from "../pages/Tab1";
import { Route, Redirect } from "react-router-dom";

interface Props {
  list: Data[];
}

const NewElementButton: React.FC = () => {
  const [createElement, setCreateElement] = useState(false);
  const [titre, setTitre] = useState<string>();
  const [description, setDescription] = useState<string>();

  return (
    <IonPage>
      <IonHeader>
        <IonToolbar>
          <IonTitle>Nouvelle note</IonTitle>
        </IonToolbar>
      </IonHeader>
      <IonContent>
        <IonHeader collapse="condense">
          <IonToolbar>
            <IonTitle size="large">Vue liste</IonTitle>
          </IonToolbar>
        </IonHeader>

        <IonItem>
          <IonInput
            value={titre}
            placeholder="Entrez le titre ici..."
            onIonChange={(e) => setTitre(e.detail.value!)}
          ></IonInput>
        </IonItem>

        <IonItem>
          <IonTextarea
            value={description}
            onIonChange={(e) => setDescription(e.detail.value!)}
            placeholder="Entrez votre texte ici..."
            id="description"
          ></IonTextarea>
        </IonItem>

        <IonButton
          color="light"
          onClick={() => {
            console.log("ajouter");
          }}
        >
          Ajouter
        </IonButton>

        <IonFab vertical="bottom" horizontal="start" slot="fixed">
          <IonFabButton color="light" href="/tab1">
            <IonIcon icon={arrowBackOutline} />
          </IonFabButton>
        </IonFab>
      </IonContent>
    </IonPage>
  );
};

export default NewElementButton;

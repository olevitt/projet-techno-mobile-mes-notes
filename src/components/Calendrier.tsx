import React from 'react';
import { Calendar, momentLocalizer } from 'react-big-calendar';
import moment from 'moment';
import {Event} from './data/Event';
import 'react-big-calendar/lib/css/react-big-calendar.css';

const localizer = momentLocalizer(moment);

interface props {
    listEvents: Event[];
  }

const Calendrier = ({listEvents }: props) => (
    <div>
    <Calendar
      localizer={localizer}
      events={listEvents}
      startAccessor="start"
      endAccessor="end"
    />
  </div>
);

export default Calendrier;


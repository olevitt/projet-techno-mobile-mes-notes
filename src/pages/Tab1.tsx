import React, { useState } from 'react';
import { IonContent, IonHeader, IonPage, IonTitle, IonToolbar } from '@ionic/react';
import ListeSimple from '../components/ListeSimple';
import './Tab1.css';
import data from '../components/data/data.json';

const Tab1: React.FC = () => {
  const [element, setElement] = useState(data.data);

  return (
    <IonPage>
      <IonHeader>
        <IonToolbar>
          <IonTitle>Mes Notes</IonTitle>
        </IonToolbar>
      </IonHeader>
      <IonContent>
        <IonHeader collapse="condense">
          <IonToolbar>
            <IonTitle size="large">Vue liste</IonTitle>
          </IonToolbar>
        </IonHeader>
        <ListeSimple list={data.data} />
      </IonContent>
    </IonPage>
  );
};

export default Tab1;
